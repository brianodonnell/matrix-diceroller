import logging, random, re
from exceptions import TooManyDice, TooManySides
from nio import SendRetryError
from markdown import markdown

logger = logging.getLogger(__name__)


async def send_text_to_room(
    client, room_id, message, notice=True, markdown_convert=True
):
    """Send text to a matrix room

    Args:
        client (nio.AsyncClient): The client to communicate to matrix with

        room_id (str): The ID of the room to send the message to

        message (str): The message content

        notice (bool): Whether the message should be sent with an "m.notice" message type
            (will not ping users)

        markdown_convert (bool): Whether to convert the message content to markdown.
            Defaults to true.
    """
    # Determine whether to ping room members or not
    msgtype = "m.notice" if notice else "m.text"

    content = {
        "msgtype": msgtype,
        "format": "org.matrix.custom.html",
        "body": message,
    }

    if markdown_convert:
        content["formatted_body"] = markdown(message)

    try:
        await client.room_send(
            room_id, "m.room.message", content, ignore_unverified_devices=True,
        )
    except SendRetryError:
        logger.exception(f"Unable to send message response to {room_id}")


def resolve(string, sorted=False):
    m = re.match("([0-9]+)?d([0-9]+)", string, re.IGNORECASE)
    if m:
        num = 1 if m.group(1) is None else int(m.group(1))
        sides = int(m.group(2))
        result = roll_dice(num, sides)
        if sorted:
            result.sort(reverse=True)
        return result
    if re.match("[0-9]+", string):
        return int(string)


def rewrite_singles(string):
    return f"1{string}" if re.match("d[0-9]+$", string) else string


def roll_dice(num, sides):
    if num > 100:
        raise TooManyDice("Cannot roll more than 100 dice")
    if sides > 100:
        raise TooManySides("Cannot roll larger than d100")
    rolls = []
    for n in range(0, num):
        rolls.append(roll_one(sides))
    return rolls


def roll_one(sides):
    return random.randint(1, sides)


def mention_link(user_id, display_name):
    return f"[{display_name}](https://matrix.to/#/{user_id})"
