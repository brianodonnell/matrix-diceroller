class TooManyDice(Exception):
    pass


class TooManySides(Exception):
    pass
