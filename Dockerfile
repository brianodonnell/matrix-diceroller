FROM python:3-slim

RUN apt update && apt install -y build-essential git  && apt-get clean

WORKDIR /opt

RUN git clone https://gitlab.matrix.org/matrix-org/olm.git

WORKDIR /opt/olm

RUN make install
RUN ldconfig

WORKDIR /opt

RUN git clone https://gitlab.com/brianodonnell/matrix-diceroller.git

WORKDIR /opt/matrix-diceroller

RUN pip install --no-cache-dir -r requirements.txt

COPY docker-run.sh .

CMD "./docker-run.sh"
