from chat_functions import (
    send_text_to_room,
    resolve,
    roll_dice,
    roll_one,
    mention_link,
    rewrite_singles,
)
from exceptions import TooManyDice, TooManySides
from nio.exceptions import OlmUnverifiedDeviceError
import re, logging

logger = logging.getLogger(__name__)


class Message(object):
    def __init__(self, client, store, config, message_content, room, event):
        """Initialize a new Message

        Args:
            client (nio.AsyncClient): nio client used to interact with matrix

            store (Storage): Bot storage

            config (Config): Bot configuration parameters

            message_content (str): The body of the message

            room (nio.rooms.MatrixRoom): The room the event came from

            event (nio.events.room_events.RoomMessageText): The event defining the message
        """
        self.client = client
        self.store = store
        self.config = config
        self.message_content = message_content
        self.room = room
        self.event = event

    async def process(self):
        syntax_chars = "()*/+-"
        text = None
        m = re.match(
            f"roll ([0-9d {syntax_chars}]+)( sorted)?$",
            self.message_content,
            re.IGNORECASE,
        )
        if m:
            text = self.roll(m, syntax_chars)

        m = re.search(
            f"exroll(er)? ([0-9]+)(.*)?$", self.message_content, re.IGNORECASE
        )
        if m:
            text = self.exroll(m)

        if text is not None:
            logger.info(text)
            await send_text_to_room(self.client, self.room.room_id, text)

    def exroll(self, m):
        sender_display_name = f"{self.room.user_name(self.event.sender)}"
        sender_user_id = f"{self.event.sender}"
        text = None
        rolls = []
        successes = 0
        double_on = 10

        dice = int(m.group(2))
        extra = m.group(3)
        m1 = re.search("double ([6789])", extra)
        if m1:
            double_on = int(m1.group(1))
        try:
            rolls = sorted(roll_dice(dice, 10), reverse=True)
            for r in rolls:
                if r >= double_on:
                    successes += 2
                elif r >= 7:
                    successes += 1
            text = f"{mention_link(sender_user_id, sender_display_name)} rolled {dice} dice {rolls} = {successes} successes"
        except (TooManyDice, TooManySide) as e:
            text = f"{mention_link(sender_user_id, sender_display_name)}: {e}"
        except Exception as e:
            print(e)
        return text

    def roll(self, m, syntax_chars):
        sender_display_name = f"{self.room.user_name(self.event.sender)}"
        sender_user_id = f"{self.event.sender}"
        text = None
        try:
            input = m.group(1)
            sorted = m.group(2) is not None
            # strip spaces
            stripped = input.replace(" ", "")
            # split string into component parts
            parts = [
                rewrite_singles(x) for x in re.split(f"([{syntax_chars}]+)", stripped)
            ]
            resolved = []
            # roll any dice sets found in the string parts and return results
            for p in parts:
                if re.match(f"[{syntax_chars}]", p):
                    resolved.append(p)
                else:
                    resolved.append(resolve(p, sorted=sorted))
            final = []
            # sum results of any dice set rolls
            for r in resolved:
                if isinstance(r, list):
                    final.append(str(sum(r)))
                # re.split will return empty strings in some situations in which case resolve() converts them to NoneType
                elif r is not None:
                    final.append(str(r))
            text = f"{mention_link(sender_user_id, sender_display_name)} rolled "
            for x in range(0, len(parts)):
                if isinstance(resolved[x], list):
                    text += f"{parts[x]} {resolved[x]} "
                else:
                    # replace asterisks with 'x' to prevent markdown weirdness
                    text += f"{parts[x]} ".replace("*", " x")
            text += f"= **{eval(' '.join(final))}**"
        except (TooManyDice, TooManySides) as e:
            text = f"{mention_link(sender_user_id, sender_display_name)}: {e}"
        except Exception as e:
            print(e)
        return text
