#!/usr/bin/env python3

import random, re, sys


def resolve(string):
    m = re.match("([0-9]+)?d([0-9]+)", string, re.IGNORECASE)
    if m:
        num = 1 if m.group(1) is None else int(m.group(1))
        sides = int(m.group(2))
        result = roll_dice(num, sides)
        return result
    if re.match("[0-9]+", string):
        return int(string)


def roll_dice(num, sides):
    rolls = []
    for n in range(0, num):
        rolls.append(roll_one(sides))
    return rolls


def roll_one(sides):
    return random.randint(1, sides)


input = " ".join(sys.argv[1:])
stripped = input.replace(" ", "")
parts = re.split("([+-])", stripped)
resolved = []
final = []
for p in parts:
    if re.match("[+-]", p):
        resolved.append(p)
    else:
        resolved.append(resolve(p))

for r in resolved:
    if isinstance(r, list):
        final.append(str(sum(r)))
    else:
        final.append(str(r))

print(" ".join(parts))
print(resolved)
print(final)
print(eval(" ".join(final)))
